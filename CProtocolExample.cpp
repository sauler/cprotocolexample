/*******************************************************************************
 *                       Copyright (C) 2016 Rafał Babiarz                      *
 *                                                                             *
 * This file is part of CProtocolExample for AQQ                               *
 *                                                                             *
 * CProtocolExample plugin is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * CProtocolExample is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include <vcl.h>
#include <windows.h>

// SDK includes
#include "SDK\PluginLink.h"
#include "SDK\PluginAPI.h"
#include "SDK\Protocol.h"

CProtocol* Protocol;

TPluginInfo PluginInfo;

int WINAPI DllEntryPoint(HINSTANCE hinst,unsigned long reason,void* lpReserved)
{
	return 1;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Load(PPluginLink Link)
{
	CPluginLink::instance()->SetLink(*Link);
	Protocol = new CProtocol();
	Protocol->JID = "example.plugin";
	Protocol->Name = "Przykładowy kontakt";
	Protocol->Prompt = "Identyfikator";
	Protocol->Transport = true;
	Protocol->Search = true;
	Protocol->GroupChat = true;
	Protocol->Description = "Przykładowa wtyczka obsługująca protokół";
	Protocol->RequiredID = true;
	Protocol->IconID = 0;
	Protocol->AccountName = "Przykładowe konto";
	Protocol->Register();


	return 0;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Unload()
{
	Protocol->Remove();
	delete Protocol;

	return 0;
}

extern "C" __declspec(dllexport) PPluginInfo __stdcall AQQPluginInfo
	(DWORD AQQVersion)
{
	PluginInfo.cbSize = sizeof(TPluginInfo);
	PluginInfo.ShortName = L"CProtocolExample";
	PluginInfo.Version = PLUGIN_MAKE_VERSION(1,0,0,0);
	PluginInfo.Description =
		L"Przykładowa wtyczka tworząca protokół wykorzystując SDK.";
	PluginInfo.Author = L"Rafał Babiarz (sauler)";
	PluginInfo.AuthorMail = L"sauler1995@gmail.com";
	PluginInfo.Copyright = L"sauler";
	PluginInfo.Homepage = L"";
	PluginInfo.Flag = 0;
	PluginInfo.ReplaceDefaultModule = 0;

	return &PluginInfo;
}

